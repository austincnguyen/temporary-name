import React, { useState, useEffect } from 'react';
import { Col, Container, Row, Card } from 'react-bootstrap';
import axios from 'axios';
import Pagination from './Pagination';
import { useParams } from 'react-router-dom';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

const Attractions = () => {
  const [attractions, setAttractions] = useState([]);
  const [error, setError] = useState(null);
  const { city } = useParams();
  const [searchCity, setSearchCity] = useState(city || 'Chicago');
  const [searchOption, setSearchOption] = useState('');
  const [sortOrder, setSortOrder] = useState('');
  const [currentPage, setCurrentPage] = useState(1);
  const citiesPerPage = 6;

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get(
          `https://testdatabase-392302.uc.r.appspot.com/attractions?cityID=${searchCity}&search=${searchOption}&sort=${sortOrder}`
        );
        setAttractions(response.data);
      } catch (error) {
        setError(error);
      }
    };

    fetchData();
  }, [searchCity, searchOption, sortOrder]);

  const changeAPI = async () => {
    try {
      const response = await axios.get(
        `https://testdatabase-392302.uc.r.appspot.com/attractions?cityID=${searchCity}&search=${searchOption}&sort=${sortOrder}`
      );
      setAttractions(response.data);
    } catch (error) {
      setError(error);
    }
  };

  const handleCitySearch = (event) => {
    const searchCity = event.target.value;
    setSearchCity(searchCity);
  };

  const handleSortOptionChange = (event) => {
    const sortOption = event.target.value;
    setSortOrder(sortOption);
  };

  const handleSearchOptionChange = (event) => {
    const searchOption = event.target.value;
    setSearchOption(searchOption);
  };


  const indexOfLastCity = currentPage * citiesPerPage;
  const indexOfFirstCity = indexOfLastCity - citiesPerPage;
  const currentAttractions = attractions.slice(indexOfFirstCity, indexOfLastCity);
  const totalPages = Math.ceil(attractions.length / citiesPerPage);

  if (error) {
    return (
      <div>
        <h1>Error</h1>
        <p>{error.message}</p>
      </div>
    );
  } else {
    return (
      <div className="backgroundAttractions">
        <br />
        <div
          style={{
            border: '10px solid white',
            padding: '1px',
            backgroundColor: 'white',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            height: '100%',
          }}
        >
          <p style={{ fontSize: 50, textAlign: 'center', fontWeight: '800', margin: 'auto' }}>
            Discover Top-Rated Attractions!
          </p>
        </div>
        <br />
        <Container>
          <Row>
            <Col>
              <div className="search-container">
                <input
                  type="text"
                  placeholder="Search"
                  value={searchOption}
                  onChange={handleSearchOptionChange}
                />
                <select onChange={handleSortOptionChange} className="sort-dropdownAttractions">
                  <option value="">Sort by</option>
                  <option value="Rating">Rating</option>
                  <option value="name">Name</option>
                </select>
                <select onChange={handleCitySearch} className="sort-dropdownAttractions">
                  <option value="">City</option>
                  <option value="Chicago">Chicago</option>
                  <option value="Austin">Austin</option>
                  <option value="Brooklyn">Brooklyn</option>
                  <option value="Charlotte">Charlotte</option>
                  <option value="Columbus">Columbus</option>
                  <option value="Dallas">Dallas</option>
                  <option value="Fort Worth">Fort Worth</option>
                  <option value="Houston">Houston</option>
                  <option value="Indianapolis">Indianapolis</option>
                  <option value="Jacksonville">Jacksonville</option>
                  <option value="Los Angeles">Los Angeles</option>
                  <option value="Manhattan">Manhattan</option>
                  <option value="New York">New York</option>
                  <option value="Philadelphia">Philadelphia</option>
                  <option value="Phoenix">Phoenix</option>
                  <option value="Queens">Queens</option>
                  <option value="San Antonio">San Antonio</option>
                  <option value="San Diego">San Diego</option>
                  <option value="San Francisco">San Francisco</option>
                  <option value="San Jose">San Jose</option>
                  <option value="The Bronx">The Bronx</option>
                </select>
              </div>
            </Col>
          </Row>
          <Row>
            {currentAttractions.map((attraction, i) => (
              <Col key={i} xs={12} sm={6} md={4}>
                <Card style={{ width: '100%', margin: '20px' }} border={'success'} bg={'light'} text={'dark'}>
                  <Card.Body>
                    <Card.Title>{attraction.name}</Card.Title>
                    <Card.Img src={attraction.image_url} style={{ width: '200px' }} />
                    <Card.Text>
                      <ul>
                        <li>Rating: {attraction.Rating}</li>
                        <li>Location: {attraction.Location}</li>
                        <li>Phone Number: {attraction.PhoneNumber}</li>
                        <li>
                          <Link
                            to={{ pathname: `/Cities`}}
                          >
                            City: {attraction.cityID}
                          </Link>
                        </li>
                        <li>
                          <Link
                            to={{ pathname: `/Reviews/${attraction.businessID}`, state: attraction.businessID }}
                          >
                            Reviews
                          </Link>
                        </li>
                      </ul>
                    </Card.Text>
                  </Card.Body>
                </Card>
              </Col>
            ))}
          </Row>
          <Pagination nPages={totalPages} currentPage={currentPage} setCurrentPage={setCurrentPage} />
        </Container>
      </div>
    );
  }
};

export default Attractions;
