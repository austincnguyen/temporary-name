import unittest
import requests
import json


class BackendTests(unittest.TestCase):
    API_URL = "https://testdatabase-392302.uc.r.appspot.com/"


    def testCitylist(self):
        r = requests.get(self.API_URL + "cities")
        self.assertEqual(r.status_code, 200)
        self.assertIsInstance(r.json(), dict)
    

    def testCityName(self):
        r = requests.get(self.API_URL + "cities?city_name=Austin")
        self.assertEqual(r.status_code, 200)
        self.assertIsInstance(r.json(), list)

    def testCityPopulation(self):
        r = requests.get(self.API_URL + "cities?population=1000000")
        self.assertEqual(r.status_code, 200)
        self.assertEqual(r.json()[0]["name"], "Brooklyn")

    def testAttractionsList(self):
        r = requests.get(self.API_URL + "attractions")
        self.assertEqual(r.status_code, 200)
        self.assertIsInstance(r.json(), dict)
    
    def testAttractionsCity(self):
        r = requests.get(self.API_URL + "attractions?cityID=Chicago")
        self.assertEqual(r.status_code, 200)
        self.assertIsInstance(r.json(), list)
    
    def testReviews(self):
        r = requests.get(self.API_URL + "reviews")
        self.assertEqual(r.status_code, 200)
        self.assertIsInstance(r.json(), list)

    def testStatsInstance(self):
        r = requests.get(self.API_URL + "reviews?sort=Rating")
        self.assertEqual(r.status_code, 200)
        self.assertIsInstance(r.json(), list)
    
    

if __name__ == "__main__":
    unittest.main()
